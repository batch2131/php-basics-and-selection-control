<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Activity</title>
    </head>
    <body>

        <h1>Letter-Based Grading</h1>
        <p><?php echo getLetterGrade(99) ?></p>
        <p><?php echo getLetterGrade(96) ?></p>
        <p><?php echo getLetterGrade(93) ?></p>
        <p><?php echo getLetterGrade(90) ?></p>
        <p><?php echo getLetterGrade(87) ?></p>
        <p><?php echo getLetterGrade(84) ?></p>
        <p><?php echo getLetterGrade(81) ?></p>
        <p><?php echo getLetterGrade(78) ?></p>
        <p><?php echo getLetterGrade(76) ?></p>
        <p><?php echo getLetterGrade(74) ?></p>

    </body>
</html>